import time, random
from service import encode_foto, send_data
import threading
from selenium import webdriver

ig_acounts = ["mim_design"]

i = 0


def add_i():
    global i
    i += 1
    print(str(i))


def ig_scrap(browser, ig_acounts):
    id_random = ig_acounts[0]
    ig_acounts.remove(id_random)
    print(len(ig_acounts))

    url = "https://www.instagram.com/" + str(id_random)

    time.sleep(random.randrange(2, 5))
    browser.get(url)
    time.sleep(random.randrange(5, 10))
    # browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    try:
        print('insta start')
        name_element = browser.find_element_by_css_selector('h1[class="rhpdm"]')

        name = name_element.text
        profile_pictures = browser.find_elements_by_css_selector('img[class="FFVAD"]')

        profile_image_urls = []
        for item in profile_pictures:
            if len(profile_image_urls) < 3:
                profile_image_urls.append(item.get_attribute('src'))

        print(name)
        print(profile_image_urls)

        if (len(ig_acounts) < 100):
            add_ig_acounts(browser, id_random, ig_acounts)

        data_sending = {
            "accounts": [
                {
                    "name": name,
                    "images": [profile_image_urls],
                    "account_provider": "ig",
                    "account_id": id_random
                }
            ],
            "token": "203D9BC00226C3B49D9DC13990182E64118B34C33099576609FE13C05EC20F06"
        }

        send_data(data_sending)
        print('insta end')
    except:
        print("fale")
        print(str(id_random))


def add_ig_acounts(browser, id_random, ig_acounts):
    acounts = browser.find_elements_by_css_selector('a[class="FPmhX notranslate  Qj3-a"]')

    hrefs = [item.get_attribute('href') for item in acounts]
    print(len(hrefs))
    print('insta ac')

    for href in hrefs:
        if "www.instagram.com/" in href:

            id_acount = (href.split("www.instagram.com/")[1]).split("/")[0]
            if id_acount != id_random:

                if id_acount not in ig_acounts:
                    ig_acounts.append(id_acount)


def create_ig_threads(browsers, acounts):
    threads = []

    for browser in browsers:
        threads.append(threading.Thread(target=ig_scrap, args=(browser, acounts,)))

    return threads