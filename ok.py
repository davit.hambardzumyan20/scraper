import time, random
from service import encode_foto, send_data
import threading

i = 0


def add_i():
    global i
    i += 1
    print(str(i))


def ok_scrap(browser, ok_acounts):
    id_random = ok_acounts[0]
    ok_acounts.remove(id_random)
    print(len(ok_acounts))

    url = "https://ok.ru/profile/" + str(id_random)
    time.sleep(random.randrange(2, 5))
    browser.get(url)
    time.sleep(random.randrange(5, 10))
    # browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    time.sleep(random.randrange(2, 5))

    name_element = browser.find_element_by_css_selector('h1[class="__user-profile-name-decorator"]')

    name = name_element.text
    print(name)
    profile_picture = browser.find_element_by_css_selector('img[id="viewImageLinkId"]')

    profile_image_url = profile_picture.get_attribute("src")

    print(profile_image_url)

    if (len(ok_acounts) < 100):
        add_ok_acounts(browser, id_random, ok_acounts)

    data_sending = {
        "accounts": [
            {
                "name": name,
                "images": [profile_image_url],
                "account_provider": "ok",
                "account_id": id_random
            }
        ],
        "token": "203D9BC00226C3B49D9DC13990182E64118B34C33099576609FE13C05EC20F06"
    }
    print(data_sending)
    send_data(data_sending)


def add_ok_acounts(browser, id_ok, ok_acounts):
    print(len(ok_acounts))

    url = "https://ok.ru/profile/" + str(id_ok) + "/friends?st._aid=NavMenu_Friend_Friends"

    browser.get(url)
    time.sleep(random.randrange(2, 4))

    acounts = browser.find_elements_by_css_selector('a[class="o"]')

    hrefs = [item.get_attribute('href') for item in acounts]

    for href in hrefs:
        if "https://ok.ru/profile/" in href:
            id_acount = (href.split("https://ok.ru/profile/")[1]).split("?st.layer.cmd")[0]
            print(id_acount)
            if id_acount not in ok_acounts:
                ok_acounts.append(id_acount)


def create_ok_threads(browsers, acounts):
    threads = []

    for browser in browsers:
        threads.append(threading.Thread(target=ok_scrap, args=(browser, acounts,)))

    return threads
