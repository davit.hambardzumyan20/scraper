import requests
from selenium import webdriver
import json
import base64
import time, random

service_url = "https://dace-37-186-121-157.ngrok.io/accounts"

def send_data(data):
    global service_url
    print(data)
    # headers = {'Content-type': 'application/json', 'Accept': '/'}
    # r = requests.post(service_url, data = json.dumps(data), headers = headers)

def encode_foto(urls):
    img_encode = []
    for url in urls:
       img_encode.append(json.dumps(base64.b64encode(requests.get(url).content).decode('utf-8'))) 
    return img_encode
    
def get_browsers(count):
    browsers = []
    for i in range(0,count):
        op = webdriver.ChromeOptions()
        op.add_argument('headless')
        browsers.append(webdriver.Chrome(options=op))
        time.sleep(10)
        print(i)
    time.sleep(random.randrange(1,3))
    return browsers    
  