from fb import create_fb_threads
from ok import create_ok_threads
from vk import create_vk_threads
from ig import create_ig_threads
from service import get_browsers
import time, random

fb_acounts = ["100068530970833", "100073439110102", "100069763279803"]
ok_acounts = ["122998942816", "122998942816"]
vk_acounts = ["id112416100", "id490652327", "id50056236"]
ig_acounts = ["trendspicks", "diala_ali", "arabicwords_0", "emirati.trends", "lhaaya"]

browsers = get_browsers(1)

while (True):
    fb_threads = create_ok_threads(browsers, fb_acounts)
    for fb_thread in fb_threads:
        fb_thread.start()
    for fb_thread in fb_threads:
        fb_thread.join()
        time.sleep(random.randrange(2, 4))

    vk_threads = create_ig_threads(browsers, vk_acounts)
    for vk_thread in vk_threads:
        vk_thread.start()
    for vk_thread in vk_threads:
        vk_thread.join()
        time.sleep(random.randrange(2, 4))

